﻿Imports System.IO
Imports System.IO.Compression
Imports System.Net
Imports Newtonsoft.Json.Linq
Public Class MainGUI
    Private Async Sub InstallButton_Click(sender As Object, e As EventArgs) Handles InstallButton.Click
        ProgressBox.Clear()
        Await Task.Run(Sub()
                           Try
                               log(ProgressBox, "Checking M-Wallet latest version")
                               getLatestVersion("m-pays", "magi", mwalletVersion, "M-Wallet [XMG] Version:", "M-Wallet Latest Version Found:")
                               log(ProgressBox, "Checking directory")
                               If Directory.Exists(Application.StartupPath + "\tmp") Then
                                   Directory.Delete(Application.StartupPath + "\tmp", recursive:=True)
                                   log(ProgressBox, Application.StartupPath + "\tmp Old Directory Deleted successfuly")
                               End If
                               Directory.CreateDirectory(Application.StartupPath + "\tmp")
                               log(ProgressBox, Application.StartupPath + "\tmp Created successfuly")
                               Directory.CreateDirectory(Application.StartupPath + "\tmp\extracted")
                               log(ProgressBox, Application.StartupPath + "\tmp\extracted Created successfuly")
                               Directory.CreateDirectory(Application.StartupPath + "\tmp\installer")
                               log(ProgressBox, Application.StartupPath + "\tmp\installer Created successfuly")
                               Directory.CreateDirectory(Application.StartupPath + "\tmp\installer\output")
                               log(ProgressBox, Application.StartupPath + "\tmp\installer\output Created successfuly")
                               log(ProgressBox, "Downloading latest version of M-Wallet")
                               downloadLatestVersion("m-pays", "magi", "win", "M-Wallet downloaded successfuly", Application.StartupPath + "\tmp\m-wallet.zip")
                               log(ProgressBox, "Downloading NSIS (Nullsoft Scriptable Install System)")
                               DownloadFile("https://netix.dl.sourceforge.net/project/nsis/NSIS%203/3.09/nsis-3.09-strlen_8192.zip", Application.StartupPath + "\tmp\nsis.zip")
                               log(ProgressBox, "Unzipping files")
                               ZipFile.ExtractToDirectory(Application.StartupPath + "\tmp\m-wallet.zip", Application.StartupPath + "\tmp\extracted")
                               ZipFile.ExtractToDirectory(Application.StartupPath + "\tmp\nsis.zip", Application.StartupPath + "\tmp\extracted\nsis")
                               log(ProgressBox, "Files unzipped successfuly")
                               log(ProgressBox, "Renaming M-Wallet Directory")
                               For Each dir As String In Directory.GetDirectories(Application.StartupPath + "\tmp\extracted")
                                   Dim dirInfo As New DirectoryInfo(dir)
                                   If dirInfo.Name.StartsWith("m-wallet-1") Then
                                       Directory.Move(dir, Path.Combine(Application.StartupPath + "\tmp\extracted", "m-wallet"))
                                   End If
                               Next
                               log(ProgressBox, "Renamed M-Wallet Directory successfuly")
                               log(ProgressBox, "Deleting all .zip files")
                               File.Delete(Application.StartupPath + "\tmp\m-wallet.zip")
                               File.Delete(Application.StartupPath + "\tmp\nsis.zip")
                               log(ProgressBox, "All .zip files deleted successfuly")
                               log(ProgressBox, "Copying M-Wallet files")
                               If Environment.Is64BitOperatingSystem Then
                                   log(ProgressBox, "System is 64-bit. Copying 64-bit version")
                                   File.Copy(Application.StartupPath + "\tmp\extracted\m-wallet\bin\64\magid.exe", Application.StartupPath + "\tmp\installer\magid.exe")
                                   File.Copy(Application.StartupPath + "\tmp\extracted\m-wallet\bin\64\m-wallet.exe", Application.StartupPath + "\tmp\installer\m-wallet.exe")
                               Else
                                   log(ProgressBox, "System is 32-bit. Copying 32-bit version")
                                   File.Copy(Application.StartupPath + "\tmp\extracted\m-wallet\bin\32\magid.exe", Application.StartupPath + "\tmp\installer\magid.exe")
                                   File.Copy(Application.StartupPath + "\tmp\extracted\m-wallet\bin\32\m-wallet.exe", Application.StartupPath + "\tmp\installer\m-wallet.exe")
                               End If
                               File.Copy(Application.StartupPath + "\tmp\extracted\m-wallet\conf\magi.conf", Application.StartupPath + "\tmp\installer\magi.conf")
                               Using stream As New FileStream(Application.StartupPath + "\tmp\installer\magi.ico", FileMode.Create, FileAccess.Write, FileShare.None), ico = TryCast(My.Resources.ResourceManager.GetObject("magi"), Icon)
                                   ico.Save(stream)
                               End Using
                               Directory.Move(Application.StartupPath + "\tmp\extracted\nsis\Bin", Application.StartupPath + "\tmp\installer\Bin")
                               Directory.Move(Application.StartupPath + "\tmp\extracted\nsis\Stubs", Application.StartupPath + "\tmp\installer\Stubs")
                               File.Copy(Application.StartupPath + "\tmp\extracted\nsis\makensis.exe", Application.StartupPath + "\tmp\installer\makensis.exe")
                               log(ProgressBox, "All files copied successfuly")
                               log(ProgressBox, "Making the installer right now")
                               log(ProgressBox, "Copying installer script from Resources")
                               File.WriteAllBytes(Application.StartupPath + "\tmp\installer\installer.nsi", My.Resources.ResourceManager.GetObject("installer"))
                               log(ProgressBox, "installer.nsi copied successfuly")
                               log(ProgressBox, "Running the [.\makensis.exe installer.nsi] command")
                               executeNSIS()
                               log(ProgressBox, "Executing M-Wallet Installer.exe. Please install it manually. Waiting for install..")
                               runInstaller()
                               log(ProgressBox, "M-Wallet install completed! [Or you just close the installer executable]")
                           Catch ex As Exception
                               log(ProgressBox, ex.Message)
                           End Try
                       End Sub)
        'SOON below..
        'Dim result As DialogResult = MessageBox.Show("Do you want to download the full XMG Blockchain?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        'If result = DialogResult.Yes Then
        '    Await Task.Run(Sub()
        '                       Try

        '                       Catch ex As Exception
        '                           log(ProgressBox, ex.Message)
        '                       End Try
        '                   End Sub)
        'Else
        '    MessageBox.Show("Operation Aborted!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End If
        SaveButton.Enabled = True
    End Sub
    Private Sub AboutButton_Click(sender As Object, e As EventArgs) Handles AboutButton.Click
        About.Show()
    End Sub
    Private Sub getLatestVersion(owner As String, repo As String, label As Label, labelValue As String, logString As String)
        Dim releaseURL As String = $"https://api.github.com/repos/{owner}/{repo}/releases/latest"
        Dim request As HttpWebRequest = WebRequest.Create(releaseURL)
        request.Method = "GET"
        request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36"
        Try
            Using response As WebResponse = request.GetResponse
                Using stream As Stream = response.GetResponseStream
                    Using reader As New StreamReader(stream)
                        Dim jsonResponse As String = reader.ReadToEnd()
                        Dim json As JObject = JObject.Parse(jsonResponse)
                        Dim tag As String = json("tag_name").ToString()
                        updateLabel(label, labelValue & " " & tag)
                        log(ProgressBox, logString & " " & tag)
                        reader.Close()
                        stream.Close()
                        response.Close()
                    End Using
                End Using
            End Using
        Catch ex As Exception
            log(ProgressBox, ex.Message)
        End Try
    End Sub
    Private Sub downloadLatestVersion(owner As String, repo As String, findJson As String, logString As String, path As String)
        Dim releaseURL As String = $"https://api.github.com/repos/{owner}/{repo}/releases/latest"
        Dim request As HttpWebRequest = WebRequest.Create(releaseURL)
        request.Method = "GET"
        request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36"
        Try
            Using response As WebResponse = request.GetResponse
                Using stream As Stream = response.GetResponseStream
                    Using reader As New StreamReader(stream)
                        Dim jsonResponse As String = reader.ReadToEnd()
                        Dim json As JObject = JObject.Parse(jsonResponse)
                        Dim assets As JArray = json("assets")
                        Dim winAsset As JObject = assets.FirstOrDefault(Function(a) a("name").ToString().ToLower().Contains(findJson))
                        Dim downloadUrl As String = winAsset("browser_download_url").ToString()
                        DownloadFile(downloadUrl, path)
                        log(ProgressBox, logString & " " & Tag)
                        reader.Close()
                        stream.Close()
                        response.Close()
                    End Using
                End Using
            End Using
        Catch ex As Exception
            log(ProgressBox, ex.Message)
        End Try
    End Sub
    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Try
            If saveLogs.ShowDialog = DialogResult.OK Then
                Using sw As New StreamWriter(saveLogs.OpenFile)
                    If sw IsNot Nothing Then
                        sw.WriteLine(ProgressBox.Text)
                        sw.Close()
                    End If
                End Using
            End If
        Catch ex As Exception
            log(ProgressBox, ex.Message)
        End Try
    End Sub
    Private Sub executeNSIS()
        Try
            Dim nsisCompilerPath As String = Application.StartupPath + "\tmp\installer\makensis.exe"
            Dim scriptFilePath As String = Application.StartupPath + "\tmp\installer\installer.nsi"

            Dim process As New Process()
            process.StartInfo.FileName = nsisCompilerPath
            process.StartInfo.Arguments = """" & scriptFilePath & """"
            process.StartInfo.UseShellExecute = False
            process.StartInfo.RedirectStandardOutput = True
            process.StartInfo.CreateNoWindow = True

            process.Start()

            Dim output As String = process.StandardOutput.ReadToEnd()

            process.WaitForExit()
            log(ProgressBox, output)
            log(ProgressBox, "NSIS Executed successfuly")
        Catch ex As Exception
            log(ProgressBox, ex.Message)
        End Try
    End Sub
    Private Sub runInstaller()
        Try
            Dim process As New Process()
            process.StartInfo.FileName = Application.StartupPath + "\tmp\installer\output\M-Wallet Installer.exe"
            process.StartInfo.UseShellExecute = False
            process.StartInfo.RedirectStandardOutput = True
            process.StartInfo.CreateNoWindow = True

            process.Start()

            Dim output As String = process.StandardOutput.ReadToEnd()

            process.WaitForExit()
        Catch ex As Exception
            log(ProgressBox, ex.Message)
        End Try
    End Sub
End Class
