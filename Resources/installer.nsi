OutFile "output\M-Wallet Installer.exe"
Name "M-Wallet Installer"
Icon "magi.ico"

InstallDir "$LOCALAPPDATA\M-Wallet"

RequestExecutionLevel user

BrandingText "M-Wallet Installer"
VIProductVersion "1.4.6.2" 
VIAddVersionKey ProductName "M-Wallet"
VIAddVersionKey Comments "M-Wallet Installer"
VIAddVersionKey Publisher "https://www.m-core.org"

Page directory
Page instfiles

Section "Main Program" SEC01
    SetOutPath $INSTDIR\bin
    File "m-wallet.exe"
    File "magid.exe"
SectionEnd

Section "Configuration Folder" SEC02
    SetOutPath $INSTDIR\conf
    File "magi.conf"
SectionEnd

Section "Creating desktop and start menu shortcut" SEC03
    CreateShortcut "$DESKTOP\M-Wallet.lnk" "$INSTDIR\bin\m-wallet.exe" 
    # Start Menu
    CreateDirectory "$SMPROGRAMS\M-Wallet"
	CreateShortcut "$SMPROGRAMS\M-Wallet\M-Wallet.lnk" "$INSTDIR\bin\m-wallet.exe"
SectionEnd

Section "Registry" SEC04
    WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\M-Wallet" "DisplayName" "M-Wallet"
    WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\M-Wallet" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
    WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\M-Wallet" "InstallLocation" "$INSTDIR"
    WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\M-Wallet" "DisplayIcon" "$INSTDIR\bin\m-wallet.exe"
    WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\M-Wallet" "Publisher" "https://www.m-core.org"
    WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\M-Wallet" "DisplayVersion" "1.4.6.2"
    WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\M-Wallet" "NoModify" 1 
    WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\M-Wallet" "NoRepair" 1
SectionEnd

Section "Uninstall" SEC05
    Delete "$INSTDIR\bin\m-wallet.exe"
    Delete "$INSTDIR\bin\magid.exe"
    Delete "$INSTDIR\conf\magi.conf"
    RMDir "$INSTDIR\conf"
    RMDir "$INSTDIR\bin"
    RMDir "$INSTDIR"
    DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\M-Wallet"
SectionEnd

Section "Creating installer" SEC06
    WriteUninstaller "uninstall.exe"
SectionEnd
