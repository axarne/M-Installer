﻿Imports System.Net
Module Stuff
    Public Sub log(txtBox As TextBox, value As String)
        updateTextBox(txtBox, "[" & Date.Now & "] " & value & vbNewLine)
    End Sub
    Public Sub DownloadFile(url As String, path As String)
        Using client As New WebClient
            client.DownloadFile(url, path)
        End Using
    End Sub
    Private Delegate Sub updateLabelDelegate(label As Label, txt As String)
    Public Sub updateLabel(label As Label, txt As String)
        If label.InvokeRequired Then
            label.Invoke(New updateLabelDelegate(AddressOf updateLabel), label, txt)
        Else
            label.Text = txt
        End If
    End Sub
    Private Delegate Sub updateTextBoxDelegate(txtBox As TextBox, txt As String)
    Public Sub updateTextBox(txtbox As TextBox, txt As String)
        If txtbox.InvokeRequired Then
            txtbox.Invoke(New updateTextBoxDelegate(AddressOf updateTextBox), txtbox, txt)
        Else
            txtbox.Text += txt
        End If
    End Sub
End Module
