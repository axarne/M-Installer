﻿Public Class About
    Private Sub About_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        AboutBox.Text += "Credits:" & vbNewLine & "Update icon by Icons8 - https://icons8.com/" &
            vbNewLine & "NSIS (Nullsoft Scriptable Install System) - https://nsis.sourceforge.io/Main_Page" &
            vbNewLine & "Newtonsoft.Json - https://github.com/JamesNK/Newtonsoft.Json"
    End Sub
End Class