# Security Policy

### Supported Versions 🛡️

This is a list of supported versions and their security support status:

| Version     | Security Updates | Small Feature Updates |
|-------------|------------------|-----------------------|
| 1.0.0.0     | ✅               | ✅                   |

If you are using a version not listed here, it may not receive security updates. Please consider upgrading to a supported version to ensure the security of your installation.


### Reporting a Vulnerability 🔒

If you discover any security vulnerabilities in this project, please report them immediately to abuse@axarne.dev.

**Response Time:** We aim to respond to all vulnerability reports within 12 hours. 🕜

Please do not disclose security vulnerabilities publicly until they have been addressed by the project maintainers.
