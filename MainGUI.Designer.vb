﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainGUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainGUI))
        Me.ProgressBox = New System.Windows.Forms.TextBox()
        Me.InstallButton = New System.Windows.Forms.Button()
        Me.mwalletVersion = New System.Windows.Forms.Label()
        Me.AboutButton = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.saveLogs = New System.Windows.Forms.SaveFileDialog()
        Me.SuspendLayout()
        '
        'ProgressBox
        '
        Me.ProgressBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.ProgressBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ProgressBox.Font = New System.Drawing.Font("Cascadia Code", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ProgressBox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.ProgressBox.Location = New System.Drawing.Point(12, 74)
        Me.ProgressBox.Multiline = True
        Me.ProgressBox.Name = "ProgressBox"
        Me.ProgressBox.ReadOnly = True
        Me.ProgressBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.ProgressBox.Size = New System.Drawing.Size(332, 115)
        Me.ProgressBox.TabIndex = 0
        '
        'InstallButton
        '
        Me.InstallButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.InstallButton.FlatAppearance.BorderSize = 0
        Me.InstallButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.InstallButton.Font = New System.Drawing.Font("Cascadia Code", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InstallButton.Location = New System.Drawing.Point(12, 45)
        Me.InstallButton.Name = "InstallButton"
        Me.InstallButton.Size = New System.Drawing.Size(148, 23)
        Me.InstallButton.TabIndex = 1
        Me.InstallButton.Text = "Download and Install"
        Me.InstallButton.UseVisualStyleBackColor = False
        '
        'mwalletVersion
        '
        Me.mwalletVersion.AutoSize = True
        Me.mwalletVersion.Font = New System.Drawing.Font("Cascadia Code", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mwalletVersion.Location = New System.Drawing.Point(9, 9)
        Me.mwalletVersion.Name = "mwalletVersion"
        Me.mwalletVersion.Size = New System.Drawing.Size(193, 15)
        Me.mwalletVersion.TabIndex = 3
        Me.mwalletVersion.Text = "M-Wallet [XMG] Version: unknown"
        '
        'AboutButton
        '
        Me.AboutButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.AboutButton.FlatAppearance.BorderSize = 0
        Me.AboutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AboutButton.Font = New System.Drawing.Font("Cascadia Code", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AboutButton.Location = New System.Drawing.Point(297, 45)
        Me.AboutButton.Name = "AboutButton"
        Me.AboutButton.Size = New System.Drawing.Size(47, 23)
        Me.AboutButton.TabIndex = 5
        Me.AboutButton.Text = "About"
        Me.AboutButton.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Cascadia Code", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(163, 15)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Installer Version: 1.0.0.0"
        '
        'SaveButton
        '
        Me.SaveButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SaveButton.Enabled = False
        Me.SaveButton.FlatAppearance.BorderSize = 0
        Me.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SaveButton.Font = New System.Drawing.Font("Cascadia Code", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.Location = New System.Drawing.Point(166, 45)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(69, 23)
        Me.SaveButton.TabIndex = 7
        Me.SaveButton.Text = "Save Logs"
        Me.SaveButton.UseVisualStyleBackColor = False
        '
        'saveLogs
        '
        Me.saveLogs.DefaultExt = "txt"
        Me.saveLogs.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*"
        '
        'MainGUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(76, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(356, 201)
        Me.Controls.Add(Me.SaveButton)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.AboutButton)
        Me.Controls.Add(Me.mwalletVersion)
        Me.Controls.Add(Me.InstallButton)
        Me.Controls.Add(Me.ProgressBox)
        Me.Cursor = System.Windows.Forms.Cursors.Cross
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "MainGUI"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Magi Installer - XMG"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ProgressBox As TextBox
    Friend WithEvents InstallButton As Button
    Friend WithEvents mwalletVersion As Label
    Friend WithEvents AboutButton As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents SaveButton As Button
    Friend WithEvents saveLogs As SaveFileDialog
End Class
