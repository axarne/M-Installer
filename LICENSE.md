Axarne Limited Use License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to use the Software for limited non-commercial purposes, including without limitation the rights to use, copy, modify, merge, and to permit persons to whom the Software is furnished to do so, provided that the following conditions are met:

1. Commercial use of the Software, including selling copies, distributing copies without owner's permission, publishing on sources other than the GitHub repository at https://github.com/AXARNE/M-Installer, and sublicensing, is prohibited without written permission from the owner. For inquiries regarding commercial use or permissions for the above, please contact support@axarne.dev.

2. Users are permitted to view the source code of the Software. Modification of the source code is not permitted without proper permission from the owner. To obtain permission for modification, please contact support@axarne.dev.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
