<h1>Magi [XMG] Wallet Installer 💿</h1>
<pre>
<i>Simple Installer Creator for Magi Core Wallet - Unofficial
© 2024 Axarne - Michael. All Rights Reserved.</i>
</pre>

<!---
![Downloads](https://img.shields.io/github/downloads/AXARNE/M-Installer/total) 
![Issues](https://img.shields.io/github/issues/AXARNE/M-Installer)
--->

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [License](#license)

## Prerequisites
Before you begin, ensure you have met the following prerequisites:

- **Recommended Font**: We recommend installing [Cascadia Code](https://github.com/microsoft/cascadia-code) font for better readability. You can download it from the [official website](https://github.com/microsoft/cascadia-code).
  
- **.NET Framework 4.7.2**: Ensure that you have .NET Framework 4.7.2 installed. You can download it from [here](https://dotnet.microsoft.com/download/dotnet-framework/net472).

- **Free Disk Space**: You'll need approximately 60 MB of free disk space.

## Installation

To install the Magi Core Wallet, follow these steps:

1. Download the latest release of the program from the [repository releases page](https://gitlab.com/axarne/M-Installer/-/releases).

2. Once downloaded, extract the contents of the downloaded archive to your preferred location. We recommend extracting it to your desktop for easy access. You can use programs like 7-Zip or WinRAR for extraction.

3. Navigate to the extracted folder and run the `Magi Installer.exe` file.

4. Follow the on-screen instructions to complete the installation process.

That's it! Your Magi Core Wallet is now installed and ready to use.

  
## License
Distributed under the Axarne Limited Use License. See [LICENSE](https://gitlab.com/axarne/M-Installer/-/blob/main/LICENSE.md) for more information.
